﻿using System;

namespace JobWorker
{
    public class KlassInfo
    {
        private const string DEFAULTMETHOD = "Perform";

        public string AssemblyName { get; private set; }

        public string AssemblyVersion { get; private set; }

        public string Klass { get; private set; }

        public string Method { get; private set; }

        public KlassInfo(string assemblyName, string assemblyVersion, string klass)
        {
            this.AssemblyName = assemblyName;
            this.AssemblyVersion = assemblyVersion;
            this.Klass = klass;
            this.Method = DEFAULTMETHOD;
        }

        public KlassInfo(string klass)
        {
            string[] parsed = klass.Split(':');
            if (parsed.Length != 3)
            {
                throw new IndexOutOfRangeException("Invalid raw job name");
            }

            this.AssemblyName = parsed[0];
            this.AssemblyVersion = parsed[1];
            this.Klass = parsed[2];
            this.Method = DEFAULTMETHOD;
        }
    }
}
