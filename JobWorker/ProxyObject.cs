﻿using QlessClient.Core.JobRunner;
using System;
using System.Reflection;

namespace JobWorker
{
    /// <summary>
    /// Marshalling object to load and tranfer a serialized payload between AppDomains (worker's and the target job's)
    /// </summary>
    class ProxyObject : MarshalByRefObject
    {
        private Type _type;

        /// <summary>
        /// Used to enable cross domain cancelling
        /// </summary>
        public InterAppDomainCancellable InterAppDomainCancellable { get; set; }

        public void LoadKlass(string AssemblyPath, string typeName)
        {
            Assembly assembly = Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + AssemblyPath); //LoadFrom loads target dependent DLLs (assuming they are in the app domain's base directory
            _type = assembly.GetType(typeName);
        
            if (_type == null)
            {
                throw new Exception("Invalid Klass type: " + typeName);
            }
        }

        /// <summary>
        /// Send a serializable payload to the BaseJobRunner with any information needed
        /// to reconstruct the QlessClient job object
        /// </summary>
        /// <param name="client"></param>
        /// <param name="jobObject"></param>
        public void InvokeMethod(string backendType, string backendInitParams, string jobObject)
        {
            BaseJobRunner obj = (BaseJobRunner)Activator.CreateInstance(_type);
            obj.CancellationToken = InterAppDomainCancellable.Token;
            //Pass in the correct version payload to the Worker lib (#futureproof)
            int version = obj.GetVersion();
            switch (version)
            {
                case 1:
                    object[] data = new object[]
                    {
                        backendType,
                        backendInitParams,                        
                        jobObject
                    };
                    obj._Perform(data);
                    break;
                default:
                    throw new Exception("Invalid worker version: " + version);
            }            
        }
    }
}
