﻿using System;
using System.Collections.Generic;
using System.Threading;
using QlessClient.Core;
using System.Threading.Tasks;
using System.Linq;
using System.Diagnostics;

namespace JobWorker { 

    public class Manager
    {
        private Dictionary<string, Worker> _workers = new Dictionary<string, Worker>();
        private Dictionary<string, Task> _tasks = new Dictionary<string, Task>();
        private bool _alive = true;
        private Client _client;
        private int _maxWorkers;        
        private int _masterId;
        private string[] _queues;
        private Thread workerThread;

        public Manager(Client client, string[] queues, int maxWorkers = 5, int masterId = 1)
        {
            _client = client;
            _maxWorkers = maxWorkers;        
            _masterId = masterId;
            _queues = queues;
        }

        public void Start()
        {
            workerThread = new Thread(StartService);
            workerThread.Name = "JobService";
            workerThread.IsBackground = true;
            workerThread.Start();
        }

        public void StartService()
        {
            StartWorkers(_maxWorkers);

            while (_alive)
            {
                try
                {
                    Thread.Sleep(100);
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occurred: '{0}", e);
                }
            }
			Task.WaitAll(_tasks.Values.ToArray());
        }

        //Helper to create a new worker thread 
        private void StartWorkers(int maxWorkers = 1)
        {
            for (int i = 1; i <= _maxWorkers; i++)
            {
                string[] names =
                {
                    System.Environment.MachineName,
                    Process.GetCurrentProcess().Id.ToString(),
                    "master",
                    _masterId.ToString(),
                    "worker",
                    i.ToString()
                };
                string name = String.Join("-", names);
                _workers[name] =   new Worker(_client, _queues, name);
				_tasks[name] = Task.Factory.StartNew(() => _workers[name].Start());
                Console.WriteLine("Master: starting " + name);
            }
        }

        public void Stop()
        {
            //flag the master to stop
            _alive = false;

            //Gracefully Stop all workers
            foreach (KeyValuePair<string, Worker> item in _workers)
            {
                Console.WriteLine("Master: stopping " + item.Key);
                item.Value.Stop();
            }
        }
    }
}