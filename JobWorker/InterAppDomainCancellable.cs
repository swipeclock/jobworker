﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Lifetime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JobWorker
{

	/// <summary>
	/// Class used to allow for cancels between app domains
	/// </summary>
	public class InterAppDomainCancellable : MarshalByRefObject, ISponsor
	{
		private readonly CancellationTokenSource cts;
		public void Cancel() { cts.Cancel(); }
		public CancellationToken Token { get { return cts.Token; } }
		private bool _releaseLease = false;
		private static readonly double  _leaseTime = System.Runtime.Remoting.Lifetime.LifetimeServices.LeaseTime.TotalSeconds;
		public InterAppDomainCancellable()
		{
			cts = new CancellationTokenSource();
		}

		public void ReleaseLease()
		{
			_releaseLease = true;
		}

		public TimeSpan Renewal(ILease lease)
		{
			if (cts.IsCancellationRequested || _releaseLease)
			{
				return TimeSpan.Zero; // don't renew
			}
			return TimeSpan.FromSeconds(_leaseTime); // renew for a second, or however long u want
		}
	}

}
