﻿using metrics4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.IO;
using QlessClient.Core;
using ExtensionMethods;
using System.Threading.Tasks;
using System.Runtime.Remoting.Lifetime;
using QlessClient.Core;

namespace JobWorker

    
{
    public class Worker
    {
        //Config file key for ModulePath location value
        const string MODULE_KEY = "worker_module_home";

        //Flag for continue processing 'while' loop
        private bool _alive = true;

        //Where the assemblies should be loaded from disk
        private string _modulePath;

        //AppDomain assembly cache
        private Dictionary<string, AppDomain> _loadedApps = new Dictionary<string, AppDomain>();

        //Worker properties
        private Client _client;
        private Logger _log = metrics4net.Logger.GetLogger;
        private string _name;
        private List<Queue> _queues;
        private Random _random = new Random();

        //Watchdog properties
        private object _locker = new object();
        private Job _job;
        private InterAppDomainCancellable _interAppDomainCancellable;

        private System.Timers.Timer _watchdog;

        /// <summary>
        /// Worker constructor with queues it should listen on.  The worker name
        /// should be globally unique
        /// </summary>
        /// <param name="client"></param>
        /// <param name="queues"></param>
        /// <param name="name"></param>
        public Worker(Client client, string[] queues, string name = "defaultworker")
        {
            this._client = client;
            this._modulePath = ConfigureModulePath();
            this._name = name;
            this._queues = ConfigureQueues(queues);
        }

        /// <summary>
        /// Helper to kill any active worker job task
        /// </summary>
        /// <param name="jid"></param>
        private void AbortThread(string jobId)
        {
           
            lock (_locker)
            {
                if (_job == null  || _interAppDomainCancellable == null ||  _job.Id != jobId)
                {
                    return;
                }
                _log.Info(this.FormatMsg("canceling active task"));
                _interAppDomainCancellable.Cancel();
            }
        }

        /// <summary>
        /// Where the job assemblies are located for retrieval
        /// </summary>
        /// <returns></returns>
        private string ConfigureModulePath()
        {
            if (ConfigurationManager.AppSettings[MODULE_KEY] != null)
            {
                return ConfigurationManager.AppSettings[MODULE_KEY];
            }

            return System.AppDomain.CurrentDomain.BaseDirectory + @"Modules";
        }

        /// <summary>
        /// Registers queues the worker should service in the job system
        /// </summary>
        /// <param name="queueNames"></param>
        /// <returns></returns>
        private List<Queue> ConfigureQueues(string[] queueNames)
        {
            List<Queue> queues = new List<Queue>();
            foreach (string name in queueNames)
            {
                if (String.IsNullOrEmpty(name))
                {
                    continue;
                }

                this._log.Info(this.FormatMsg("registering queue " + name));
                Queue q = new Queue(name, this._client);
                queues.Add(q);
            }

            return queues;
        }

        /// <summary>
        /// Checks, creates, and loads any AppDomain needed for Job Assemblies.  One AppDomain is
        /// created per Assembly and then cached in a Dictionary for later retrieval.  Constantly
        /// loading and Disposing of AppDomain is extremely expensive.
        /// </summary>
        /// <param name="klassInfo"></param>
        /// <returns></returns>
        private AppDomain GetAppDomain(KlassInfo klassInfo)
        {
            string nameKey = klassInfo.AssemblyName + "-" + klassInfo.AssemblyVersion;
            if (this._loadedApps.ContainsKey(nameKey))
            {
                //We've loaded this before.  Return cached assembly
                return this._loadedApps[nameKey];
            }

            //Load the assemblies (expensive!)
            string assemblyPath = this._modulePath + @"\" + klassInfo.AssemblyName + @"\" + klassInfo.AssemblyVersion + @"\";
            if (!Directory.Exists(assemblyPath))
            {
                throw new FileNotFoundException(assemblyPath + " does not exist");
            }

            this._log.Info(this.FormatMsg("loading '" + nameKey + "' assembly into new AppDomain."));
            AppDomainSetup setup = new AppDomainSetup();
            setup.ApplicationBase = assemblyPath;
            this._loadedApps[nameKey] = AppDomain.CreateDomain(nameKey, null, setup);

            //Return newly loaded assembly            
            return this._loadedApps[nameKey];
        }

        /// <summary>
        /// A Random grace period generator to allocate additional time for
        /// any timed operations.  This helps account for latency in heartbeats,
        /// time skew, etc
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        private int GetGracePeriod(int min = 1000, int max = 3000)
        {
            Random rnd = new Random();
            return (int)rnd.Next(min, max);
        }

        /// <summary>
        /// @TODO temporary log format helper until metrics4net is extended
        /// to support Format
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private string FormatMsg(string message)
        {
            return string.Format("{0}: {1}", this._name, message);
        }

        /// <summary>
        /// Helper to get logging context for Job items
        /// </summary>
        /// <param name="job"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        private object GetJobContext(Job job, string status)
        {
            return new
            {
                status = status,
                jid = job.Id,
                klass = job.Klass,
                worker = this._name
            };
        }

        /// <summary>
        /// Gets the number of milliseconds until a job is considered expired
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        private long GetTTL(double expires)
        {
            TimeSpan span = JobExtensions.FromUnixTime(Convert.ToInt64(expires)) - DateTime.UtcNow;
            long ttl = Convert.ToInt64(span.TotalMilliseconds);

            return ttl + this.GetGracePeriod();
        }

        /// <summary>
        /// Helper to get logging context for Worker item
        /// </summary>
        /// <returns></returns>
        private object GetWorkerContext()
        {
            return new
            {
                worker = this._name
            };
        }

        private object ExecuteJob(Job job)
        {
            object jobError = null;
            Task task;
            try
            {
            
                //Start Metrics
                DateTime start = DateTime.UtcNow;
                string status = "ok";
				ILease lease = null;

				try
                {
                    lock (_locker)
                    {
                        _job = job;
                        //this._currentJob = "SampleTask.1-0-0-0.SampleTask.Run";                                      
                        KlassInfo klassInfo = new KlassInfo(job.Klass);
                        AppDomain domain = this.GetAppDomain(klassInfo);

                        ProxyObject proxyObject = (ProxyObject)domain.CreateInstanceFromAndUnwrap(typeof(ProxyObject).Assembly.Location, typeof(ProxyObject).FullName);
                        proxyObject.LoadKlass(klassInfo.AssemblyName + ".dll", klassInfo.AssemblyName + "." + klassInfo.Klass);
                        _interAppDomainCancellable = (InterAppDomainCancellable)domain.CreateInstanceFromAndUnwrap(typeof(InterAppDomainCancellable).Assembly.Location, typeof(InterAppDomainCancellable).FullName);
						lease = _interAppDomainCancellable.InitializeLifetimeService() as ILease;
						lease.Register(_interAppDomainCancellable);


						var primaryCts = new CancellationTokenSource();
                        // Cancel the secondary when the primary is cancelled.
                        primaryCts.Token.Register(() => _interAppDomainCancellable.Cancel());
                        proxyObject.InterAppDomainCancellable = _interAppDomainCancellable;

                        task = Task.Factory.StartNew(() => proxyObject.InvokeMethod(
                                        this._client.GetBackendType(),
                                        JsonConvert.SerializeObject(this._client.GetBackendInitParams()),
                                        JsonConvert.SerializeObject(_job)
                                    ), TaskCreationOptions.AttachedToParent);

                    }
                    StartWatchdog(job, job.Expires);
                    task.Wait();	
					_interAppDomainCancellable.ReleaseLease();
				}
                catch (Exception e)
                {
                    status = "error";
                    _log.Error(FormatMsg("Job thread  error occurred: " + e), GetWorkerContext());
                    jobError = e;
                    _log.Info(this.FormatMsg("job failed with err: " + jobError.ToString()), GetWorkerContext());
                }
                finally
                {
					if (lease != null && _interAppDomainCancellable != null)
					{
						lease.Unregister(_interAppDomainCancellable);
					}

                    //Stop Metrics
                    object jobContext = this.GetJobContext(job, status);
                    _log.Timer("job.time", DateTime.UtcNow.Subtract(start).Milliseconds, jobContext);
                    _log.Count("job.count", 1, jobContext);
                }
            }
            catch (Exception e)
            {
                this._log.Error(this.FormatMsg("an error occurred: " + e));
                jobError = e;
            }
            finally
            {
                if (jobError != null)
                {
                    this._log.Info(this.FormatMsg("job failed with err: " + jobError.ToString()), this.GetWorkerContext());
                }
            }

            return jobError;
        }
        /// <summary>
        /// Helper to transfrom a pub\sub event into a Dictionary
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private Dictionary<string, string> ParseEvent(string message)
        {
            Dictionary<string, string> result = null;
            try
            {
                result = JsonConvert.DeserializeObject<Dictionary<string, string>>(message);
            }
            catch (Exception ex)
            {
                this._log.Debug(this.FormatMsg(" error parsing event message e:" + ex));
            }

            return result;
        }

        /// <summary>
        /// Event handler for when the job system has heartbeated a job.
        /// The watchdog is restarted which will kill any current thread
        /// </summary>
        /// <param name="jobEvent"></param>
        private void ProcessHeartbeatEvent(Dictionary<string, string> jobEvent)
        {
            string[] requiredKeys = { "event", "jid", "expires" };
            foreach (string e in requiredKeys)
            {
                if (!jobEvent.ContainsKey(e))
                {
                    return;
                }
            }
            if (_job.Id == jobEvent["jid"])
            {
                this.StartWatchdog(_job, Convert.ToDouble(jobEvent["expires"]));
            }
        }

        /// <summary>
        /// Event handler for when a worker loses a lock on the job.
        /// The worker should kill the active thread
        /// </summary>
        /// <param name="jobEvent"></param>
        private void ProcessLockLostEvent(Dictionary<string, string> jobEvent)
        {
            string[] requiredKeys = { "event", "jid" };
            foreach (string e in requiredKeys)
            {
                if (!jobEvent.ContainsKey(e))
                {
                    return;
                }
            }

            this.AbortThread(jobEvent["jid"]);
        }

        /// <summary>
        /// Randomly selects a queue that the worker is registered for and attempts to pop a job.
        /// If no job is found then the next queue is tried until a single job is found to process
        /// or all queues have been tried
        /// </summary>
        /// <returns>
        /// A Job to process
        /// </returns>
        private Job Reserve()
        {
            try
            {
                List<Queue> sourceQueues = new List<Queue>(this._queues);
                while (sourceQueues.Count > 0)
                {
                    int index = this._random.Next(sourceQueues.Count);
                    Queue queue = sourceQueues[index];
                    sourceQueues.RemoveAt(index);

                    List<Job> resultList = queue.Pop(this._name, 1);
                    if (resultList == null || resultList.Count == 0)
                    {
                        continue;
                    }

                    return resultList[0];
                }
            }
            catch (Exception e)
            {
                this._log.Error(this.FormatMsg("error reserving job: " + e));
            }

            return null;
        }

        /// <summary>
        /// Schedules a job for retries.  There is a possibility that the job will not be in a 
        /// retryable state by the time this code executes.  In that case an exception would be
        /// returned.  We no longer have a lock on the job so we can't do anything about it
        /// </summary>
        /// <param name="job"></param>
        private void RetryJob(Job job)
        {
            try
            {
                //job.Fail(err); #@TODO DAN for discussion fail vs retry only on except? always retry? Failures are forever
                job.Retry(this._name + " triggered retry");
            }
            catch (Exception e)
            {
                this._log.Error(this.FormatMsg("an error occured trying to queue a retry: " + e));
            }
        }

        /// <summary>
        /// Subscribes to worker name specific events and passes them to 
        /// the event handler
        /// </summary>
        private void StartEventListener()
        {
            this._log.Info(this.FormatMsg("watchdog subscribing to worker events"));
            this._client.Subscribe("ql:w:" + this._name, (channel, message) =>
            {
                this._log.Info(this.FormatMsg("watchdog received '" + channel + "' on channel '" + message + "'"));
                this.SubscriberEventHandler(channel, message);
            });
        }

        /// <summary>
        /// Unload the registered channels for this worker
        /// </summary>
        private void StopEventListener()
        {
            this._log.Info(this.FormatMsg("watchdog unsubscribing from worker events"));
            this._client.Unsubscribe("ql:w:" + this._name);
        }

        /// <summary>
        /// The Watchdog is in job of killing a Worker job thread if the timeout is exceeded.
        /// This is accomplished via a delayed thread.  If the Watchdog needs to wait longer 
        /// because the timeout changes then recalling the method will remove the current
        /// Watchdog and set a new Watchdog with the appropriate expiry time\delay
        /// </summary>
        /// <param name="job"></param>
        private void StartWatchdog(Job job, double expires)
        {
            if (this._watchdog != null)
            {
                this._watchdog.Stop();
            }

            this._watchdog = new System.Timers.Timer();
            this._watchdog.Interval = this.GetTTL(expires);
            this._watchdog.AutoReset = false;
            this._watchdog.Elapsed += (s, args) =>
            {
                try
                {
                    this._log.Debug(this.FormatMsg("watchdog barking for " + (job.Id)));
                    this.AbortThread(job.Id);
                }
                catch (Exception e)
                {
                    this._log.Error(this.FormatMsg("WATCHDOG error " + e));
                }
            };

            this._log.Debug("scheduling watchdog for " + (this._watchdog.Interval / 1000) + " seconds");
            this._watchdog.Start();
        }

        /// <summary>
        /// Attempts to parse an events and route to the correct handler
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="message"></param>
        private void SubscriberEventHandler(string channel, string message)
        {
            Dictionary<string, string> jobEvent = this.ParseEvent(message);

            if (jobEvent == null)
            {
                return;
            }

            string eventName = "";
            jobEvent.TryGetValue("event", out eventName);
            switch (eventName)
            {
                case "heartbeat":
                    this.ProcessHeartbeatEvent(jobEvent);
                    break;
                case "lock_lost":
                case "canceled":
                    this.ProcessLockLostEvent(jobEvent);
                    break;
                default:
                    this._log.Debug(this.FormatMsg("no processing rules exist for received event"));
                    break;
            }
        }

        /// <summary>
        /// The main work loop for this worker.  Will continuously process jobs until stopped via the Stop() method.
        /// Once a job is popped a thread is created and the appropriate job appdomain is created and called.
        /// If a job throws for whatever reason the client will signal the QlessApi to retry the job
        /// </summary>                       
        public void Start()
        {
            this._log.Info(this.FormatMsg("started"), this.GetWorkerContext());
            this.StartEventListener();
            while (this._alive)
            {
                Job job = this.Reserve();
                if (job == null)
                {
                    Thread.Sleep(3000);
                    this._log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " " + this.FormatMsg("no work found"), this.GetWorkerContext());
                    continue;
                };

                object jobError = this.ExecuteJob(job);

                if (jobError == null)
                {
                    //If no error we don't need to attempt retries
                    continue;
                }

                this.RetryJob(job);
            }

            this.StopEventListener();
            this._log.Info(this.FormatMsg("worker exited."));
        }

        /// <summary>
        /// Set the _alive to false to stop the worker Start() loop on its *next* iteration.
        /// This allows for workers to gracefully shutdown.
        /// </summary>
        public void Stop()
        {
            this._alive = false;
        }

    }
}
