﻿using QlessClient.Core;
using QlessClient.Core.Backend;
using System;
using System.Configuration;
using Topshelf;

namespace JobWorker
{

    public class Program
    {
        public static void Main()
        {
            HostFactory.Run(x =>                                 
             {
                 x.Service<Manager>(s =>                        
                 {
                     IBackend backend = Program.GetBackend();
                     string[] queues = Program.GetQueues();
                     int maxWorkers = Program.GetWorkerCount();
                     int masterId = Program.GetMasterId();
                     s.ConstructUsing(name => new Manager(
                         new Client(backend),
                         queues,
                         maxWorkers,
                         masterId
                     ));                          
                     s.WhenStarted(mgr => mgr.Start());              
                     s.WhenStopped(mgr => mgr.Stop());               
                 });
                x.RunAsLocalSystem();                            
            
                 x.SetDescription("Job System Worker");        
                 x.SetDisplayName("Job System Worker");                       
                 x.SetServiceName("JobSystemWorker");                       
            });            
        }

        /// <summary>
        /// Helper to fetch the appropriate backend instance.  Currently only a qless
        /// instance is supported
        /// </summary>
        /// <returns></returns>
        private static IBackend GetBackend()
        {
            string backend = "qless";
            if (ConfigurationManager.AppSettings["backend"] != null)
            {
                backend = ConfigurationManager.AppSettings["backend"];
            }

            switch (backend)
            {
                case "qless":
                    //Hardcoded backend config for now.  Can expand later if we need generic backend config
                    string opt = Program.GetBackendOpts();
                    if (opt != null)
                        return new QlessBackend(opt);
                    else
                        throw new Exception("No Qless Backend Specified.");
                default:
                    throw new Exception("Invalid backend name specified");
            }
        }

        /// <summary>
        /// Potential multiple backend support.  Can pass in string params to IBackend instances via config
        /// </summary>
        /// <returns></returns>
        private static string GetBackendOpts()
        {

            if (ConfigurationManager.AppSettings["backend_opt"] == null)
            {
                return null;                
            }

            return ConfigurationManager.AppSettings["backend_opt"];
        }
      
        /// <summary>
        /// Helper to get the MasterId from config or use a sane default
        /// </summary>
        /// <returns></returns>
        private static int GetMasterId()
        {
            //@TODO: Random Master could be an improvement to prevent worker name collisions
            int id = 1;
            if (ConfigurationManager.AppSettings["master_id"] != null)
            {
                id = Convert.ToInt32(ConfigurationManager.AppSettings["master_id"]);
            }

            return id;
        }

        /// <summary>
        /// Helper to get the number of workers to spawn or use a sane default
        /// </summary>
        /// <returns></returns>
        private static int GetWorkerCount()
        {
            int count = 5;
            if (ConfigurationManager.AppSettings["worker_count"] != null)
            {
                count = Convert.ToInt32(ConfigurationManager.AppSettings["worker_count"]);
            }

            return count;
        }

        /// <summary>
        /// Helper to get the queue(s) the workers should listen on.
        /// If no queues are provided it default to "default"
        /// </summary>
        /// <returns></returns>
        private static string[] GetQueues()
        {
            if (ConfigurationManager.AppSettings["worker_queue"] == null)
            {
                return new string[] { "default" };
                
            }

            return ConfigurationManager.AppSettings["worker_queue"].Split(',');
        } 
    }  
}
