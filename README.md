### JobWorker
- Manages execution of .NET qless jobs

### Modules
- Modules are dedicated qless job projects.  Their installation must follow the following directory convention:

  ```
<worker_module_home>\<assembly name>\<assembly version>\<project bin contents>
```

  An example:
 ```
 C:\JobWorkerModules\sampletask\1.0.0.0
 ```

- **NOTE**:
  This was done prior to my understanding of nuget packages and their structure.  We may move this to be directly compatible with nuget packages.  Right now the deployment is manual

### Module Execution and Klass:
  - For a job to be loaded the Klass name in the backend must match the following convention:
  ```
  <assembly name>:<assembly version>:<class name>
  ```

  An example:
  ```
  SampleTask:1.0.0.0:AllGood
  ```
  
  - Where the targeted class (AllGood) inherits from `QlessClient.BaseJobRunner` **and** implements a `Perform` method

### Configuration
- backend
  - only valid backend is `qless`
- backend_opt
  - options for the specified backend
  - qless example: `LinuxHost:6378`
- master_id
  - the id of the master
  - useful for distinguishing multiple running JobWorkers
- worker_count
  - how many workers to run 
- worker_queue
  - comma separated string of qless queues to pull work from
- worker_module_home
  - the directory were modules our located
  - modules must be located inline the the convention notes in [Modules]()

### Manager:
 - spawns number of workers specified by `worker_count`
 - gracefully stop workers on exit.  Allows them to complete their current job (if any)
 - in charge of creating the number of workers specified in *worker_count*.  

### Worker:
 - pops a single job at a time from one of the queues specified in `worker_queue`
 - creates a *WatchDog* that will kill a job if its TTL expires
 - subscribes to the qless redis channel for important event:
   - `heartbeat`:
     - the jobs TTL has been reset and updates the WatchDog
   - `locklost`
     - the worker is no longer responsible for the job.  This can be from timeouts or stalls.  The worker kills the job task
   - `canceled`
     - the job has been explicitly canceled.  The worker kills the running task   
